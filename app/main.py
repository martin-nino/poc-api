from fastapi import FastAPI
import redis
import time

app = FastAPI()
db = redis.Redis(host='redis-service', port=6379, db=0)


@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.get("/items/{item_id}")
def read_item(item_id: int, q: str = None):
    return {"item_id": item_id, "q": q}


@app.get("/write")
def write_redis(key: str, value: str):
    db.set(key, value)
    return "Ok"


@app.get("/read")
def read_redis(key: str):
    value = db.get(key)
    return value


@app.get("/sleep")
def sleep():
    time.sleep(1)
    return {"Hello": "World"}
