FROM tiangolo/uvicorn-gunicorn-fastapi:python3.7

COPY ./app /app

RUN pip install --upgrade pip && \
  pip install --no-cache-dir -r requirements.txt